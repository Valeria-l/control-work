#include <iostream>
#include <string>
#include <algorithm>
#include <fstream>

using namespace std;

int nextSmallerThan(int);
void nextSmallerThan(string, string);
void nextSmallerThanTests();
bool nextSmallerThanTests(string, string);

int main()
{
	nextSmallerThanTests();
	nextSmallerThan("input.txt", "result.txt");
	nextSmallerThanTests("input.txt", "result.txt");
	system("pause");
}

int nextSmallerThan(int number)
{
	if (number < 10) {
		return -1;
	}

	int lastValue = number % 10; 
	int secondLastValue = (number / 10) % 10;

	if (lastValue < secondLastValue) {
		int swap = (number / 100) * 100 + lastValue * 10 + secondLastValue; 
		return swap;
	}
	else {
		int numberDigits = 0;
		int n = number;
		while (n > 0) {
			numberDigits++;
			n /= 10;
		}

		int* values = new int[numberDigits];
		n = number;
		for (int i = 0; i < numberDigits; i++) {
			values[i] = n % 10;
			n /= 10;
		}

		for (int i = numberDigits - 2; i >= 0; i--) {
			if (values[i] < values[i + 1]) {//������
				int n = values[i];
				values[i] = values[i + 1];
				values[i + 1] = n;

				int result = 0;// ��� ����� ��� �� �������� ����� 
				for (int j = 0; j < numberDigits; j++) {
					result = result * 10 + values[j];
				}

				delete[] values; 
				return result;
			}
		}

		delete[] values; 
		return -1;
	}
}

void nextSmallerThan(string input, string output)
{
	int number = stoi(input);
	int result = nextSmallerThan(number);
	if (result == -1) {
		output = "No";
	}
	else {
		output = to_string(result);
	}
}

bool nextSmallerThanTests(string input, string output)
{
	ifstream in1(input);
	ifstream in2(output);
	string source1 = "";
	string source2 = "";
	while (getline(in1, source1) && getline(in2, source2))
	{
		int n1 = stoi(source1);
		int n2 = stoi(source2);
		if (n1 != n2)
		{
			in1.close();
			in2.close();
			return false;
		}
	}
	in1.close();
	in2.close();
	return true;
}

void nextSmallerThanTests()
{
	cout << "Test " << (nextSmallerThan(21) == 12 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(531) == 513 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(2071) == 2017 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(9) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(111) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(135) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(1027) == -1 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(1113211111) == 1113121111 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(91234567) == 79654321 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(173582) == 173528 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(4321234) == 4314322 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThan(2147483647) == 2147483476 ? "Passed." : "Failed.") << endl;
	cout << "Test " << (nextSmallerThanTests("result.txt", "output.txt") ? "Passed." : "Failed.") << endl;
}