#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct Map
{
	char symbol;
	int number;
	static const int count = 26;
};

Map* createMap();
void writeMapToFile(string, Map*);
void displayContentFileWithMap(string);
void encoding(string, string, Map*);
void encoding(string, string, Map*);
void decoding(string, string, Map*);
Map* createDectionary(string);
int encoding(char, Map*);
char decoding(int, Map*);

int main()
{
	Map* map = createMap();
	writeMapToFile("codes", map);
	displayContentFileWithMap("codes");
	encoding("input.txt", "result.txt", map);
	decoding("result.txt", "output.txt", map);
}

Map* createMap()
{
	//TODO
}

void writeMapToFile(string fileName, Map* map)
{
	//TODO
}

void displayContentFileWithMap(string fileName)
{
	//TODO
}

Map* createDectionary(string fileName)
{
	//TODO
}

int encoding(char symbol, Map* map)
{
	//TODO
}

void encoding(string input, string output, Map* map)
{
	//TODO
}

void decoding(string input, string output, Map const* map)
{
	//TODO
}

char decoding(int item, Map const* map)
{
	//TODO
}