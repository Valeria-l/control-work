#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int binaryToDecimal(string);
int calculateSum(string);
int calculateSumFromFile(string);
void calculateSumTest(string);

int main()
{
	calculateSumTest("test.txt");
}

int calculateSum(string source)
{
	int sum = 0;
	string numbers = "";
	for (int i = 0; i < source.length(), i++) {
		if (source[i] >= '0' && source[i] <= '9')
		{
			numbers += source[i];
		}
		else {
			source.erase(i, 1);
			i--;
		}
		sum = !numbers.empty() ? std::stoi(numbers) : 0;

		return sum;

	}
}

int calculateSumFromFile(string fileName)
{
	ifstream file(test.txt);
	int sum = 0;
	string fileNumber = "";

}

int binaryToDecimal(string binary)
{
	int binary = 0;                                 
	for (int i = 0; i < binary.length(); i++) {   
		if (binary[i] == '1') {                  
			binary += pow(2, binary.length() - i - 1); 
	}
	return binary;
}

void calculateSumTest(string fileName)
{
	bool result = calculateSum("1+-0100+*** 1000") == 13;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1000001+-1000+* 100** 1--- 0000001") == 79;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("10000000000011111+11+*** 1111000111") == 66537;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1111111111111000011111+-11111111111+*   1111** -22--- ") == 4195885;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSumFromFile(fileName) == 4262515;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
}